package com.donkey.applicaiton.marketweb.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

@RestController
@RequestMapping("LongPolling")
public class DeferredResultController1 {

    @GetMapping("/getLongPollingResult")
    public DeferredResult<String> getLongPollingResult(Long sleepTime) {
        DeferredResult<String> result = new DeferredResult<>(5000L, "timeout");

        //模拟另一个线程，设置返回值
        new Thread(() -> {
            try {
                Thread.sleep(sleepTime);
                result.setResult("get processor result");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

        return result;
    }


}
