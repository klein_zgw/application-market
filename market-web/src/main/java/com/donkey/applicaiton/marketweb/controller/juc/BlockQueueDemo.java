package com.donkey.applicaiton.marketweb.controller.juc;

import com.alibaba.ttl.TransmittableThreadLocal;

import java.util.ArrayDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 手动实现阻塞队列
 *
 * @param <T>
 */
public class BlockQueueDemo<T> {
    public static void main(String[] args) throws InterruptedException {
        //BlockQueueDemo<Integer> queue = new BlockQueueDemo<>(5);
        //Thread thread = new Thread(() -> {
        //    for (int i = 0; i < 10; i++) {
        //        System.out.println(Thread.currentThread().getName() + " 往队列当中加入数据：" + i);
        //        queue.put(i);
        //    }
        //}, "生产者");
        //
        //Thread thread1 = new Thread(() -> {
        //    for (int i = 0; i < 10; i++) {
        //        System.out.println(Thread.currentThread().getName() + " 从队列当中取出数据：" + queue.take());
        //        System.out.println(Thread.currentThread().getName() + " 当前队列当中的数据：" + queue);
        //    }
        //}, "消费者");
        //thread.start();
        //TimeUnit.SECONDS.sleep(3);
        //thread1.start();

        BlockQueueDemo<Long> queue = new BlockQueueDemo<>(5);
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        executorService.execute(() -> {
            queue.put(Thread.currentThread().getId());
        });
        ExecutorService executorService1 = Executors.newFixedThreadPool(10);
        executorService1.execute(() -> {
            Long take = queue.take();
            System.out.println("消费者取出" + take);
        });

    }

    private final ReentrantLock lock = new ReentrantLock();
    private final Condition notEmpty = lock.newCondition();
    private final Condition notFull = lock.newCondition();
    //容量
    private int capacity;
    private ArrayDeque<T> queue;

    public BlockQueueDemo(int capacity) {
        this.capacity = capacity;
        queue = new ArrayDeque<>(capacity);
    }

    public void put(T t) {
        lock.lock();

        try {
            while (queue.size() >= this.capacity) {
                notFull.await();
            }

            queue.add(t);
            System.out.println("生产者放入" + Thread.currentThread().getId());
            notEmpty.signal();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public T take() {
        lock.lock();

        try {
            while (queue.isEmpty()) {
                notEmpty.await();
            }

            T t = queue.poll();
            notFull.signal();
            return t;
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            lock.unlock();
        }
    }

}
