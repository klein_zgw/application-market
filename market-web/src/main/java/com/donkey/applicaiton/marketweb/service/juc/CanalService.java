package com.donkey.applicaiton.marketweb.service.juc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.*;

@Service
public class CanalService {
    private volatile boolean running = false;
    private Thread thread;

    //@Autowired
    //private CanalConnector canalConnector;

    public void handle() {
        //连接canal
        while(running) {
            //业务处理
        }
    }

    public void start() {
        thread = new Thread(this::handle, "name");
        running = true;
        thread.start();
    }

    public void stop() {
        if(!running) {
            return;
        }
        running = false;
    }
}
