package com.donkey.applicaiton.marketweb.service.juc;

public class MyThread implements Runnable {
    private int number;
    private volatile boolean isStop;//使用volatile关键字修饰，可以在多线程之间共享，成员变量来控制线程的停止

    @Override
    public void run() {
        //super.run();
        System.out.println(Thread.currentThread().getName() + " == 进入");
        while (!isStop) {
            synchronized (MyThread.class) {
                try {
                    Thread.sleep(1000);
                    System.out.println(Thread.currentThread().getName() + " == " + number);
                    number++;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 设置线程标识
     *
     * @param isStop true：停止 false：不停止
     */
    public void stopThread(boolean isStop) {
        this.isStop = isStop;
    }

    public static void main(String[] args) {
        try {
            //自定义线程
            MyThread runnable = new MyThread();
            Thread thread = new Thread(runnable, "子线程");
            thread.start();

            //睡5秒
            Thread.sleep(5000);

            //停止线程
            runnable.stopThread(true);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}


