package com.donkey.application.market.common.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author 戴着假发的程序员
 * @todo 就是一个分布式锁的实现 20211117
 */
@Component
public class RedisLock {

    private String lockKey = "lock";
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 尝试获取锁
     *
     * @param requestId  请求id
     * @param expireTime 过期时间  单位毫秒
     * @return
     */
    public boolean tryGetLock(String requestId, int expireTime) {
        //这里利用redis的set命令
        //使用reids保证原子操作（判断是否存在，添加key，设置过期时间）
        while (true) {
            if (stringRedisTemplate.boundValueOps(lockKey).
                    setIfAbsent(requestId, expireTime, TimeUnit.SECONDS)) {
                return true;
            }
        }
    }

    public boolean releaseLock(String requestId) {
        //这里使用Lua脚本保证原子性操作
        String script = "if  redis.call('get', KEYS[1]) == ARGV[1] then " +
                "return redis.call('del', KEYS[1]) " +
                "else return 0 end";
        DefaultRedisScript<Long> redisScript = new DefaultRedisScript<>(script, Long.class);
        return new Long(1).
                equals(stringRedisTemplate.
                        execute(redisScript, Collections.singletonList(lockKey), requestId));
    }

    /**
     * 创建续命子线程
     *
     * @param time      操作预期耗时
     * @param requestId 唯一标识
     * @return
     */
    public Thread createLifeThread(int time, String requestId) {
        return new Thread(() -> {
            while (true) {
                try {
                    TimeUnit.SECONDS.sleep(time * 2 / 3);
                    //重置时间
                    String script = "if redis.call('get', KEYS[1]) == ARGV[1] then " +
                            "return redis.call('expire', KEYS[1],ARGV[2]) " +
                            "else return '0' end";
                    List args = new ArrayList();
                    args.add(requestId);
                    args.add(time);
                    DefaultRedisScript<Long> redisScript =
                            new DefaultRedisScript<>(script, Long.class);
                    stringRedisTemplate.execute(redisScript,
                            Collections.singletonList(lockKey), args);
                } catch (Exception e) {
                    return;
                }

            }
        });
    }


}
