package com.donkey.application.market.common.mq;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.apache.rocketmq.spring.core.RocketMQPushConsumerLifecycleListener;
import org.springframework.stereotype.Component;

@Component
public class Consumer<T> implements RocketMQListener<T>, RocketMQPushConsumerLifecycleListener {

    @Override
    public void onMessage(T message) {
        try{
               doSomething();
        }catch(Throwable e){
            //throw exception
        }
    }

    private void doSomething() {

    }

    @Override
    public void prepareStart(DefaultMQPushConsumer consumer) {
        consumer.setConsumeThreadMax(50);
        consumer.setConsumeThreadMin(50);
    }
}
