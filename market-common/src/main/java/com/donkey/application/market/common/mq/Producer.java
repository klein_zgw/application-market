package com.donkey.application.market.common.mq;

import org.apache.commons.lang3.StringUtils;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.UUID;

@Component
public class Producer {

    @Value("mq.topic")
    private String topic;

    @Resource
    private RocketMQTemplate template;

    /**
     * 同步
     * @param tags
     * @param msgBody
     */
    public void sendSync(String tags, Object msgBody){
        checkParams(msgBody);
        sendSyncHandler(tags, msgBody, false, "");
    }

    /**
     * 异步
     *
     * @param tags
     * @param msgBody
     */
    public void sendAsync(String tags, Object msgBody) {
        checkParams(msgBody);
        sendAsyncHandler(tags, msgBody, false, "");
    }

    /**
     * 异步
     *
     * @param tags
     * @param msgBody
     * @param msgKey
     */
    public void sendAsync(String tags, Object msgBody, String msgKey) {
        checkParams(msgBody);
        sendAsyncHandler(tags, msgBody, true, msgKey);
    }

    /**
     * 单向One Way
     *
     * @param tags
     * @param msgBody
     */
    public void sendOneWay(String tags, Object msgBody) {
        checkParams(msgBody);
        sendOneWayHandler(tags, msgBody, false, "");

    }

    /**
     * 单向One Way
     *
     * @param tags
     * @param msgBody
     * @param msgKey
     */
    public void sendOneWay(String tags, Object msgBody, String msgKey) {
        checkParams(msgBody);
        sendOneWayHandler(tags, msgBody, true, msgKey);
    }

    private void sendSyncHandler(String tags, Object msgBody, boolean isNeedMsgKey, String msgKey) {
        try{
            Message msg = buildMsg(msgBody, isNeedMsgKey, msgKey);
            if (null == msg) {
                //todo throw exception
            }
            template.syncSend(getDestination(tags), msg);
        }catch(Throwable e){
            //todo throw exception
        }
    }

    private void sendAsyncHandler(String tags, Object msgBody, boolean isNeedMsgKey, String msgKey) {
        Message msg = buildMsg(msgBody, isNeedMsgKey, msgKey);

        if (null == msg) {
            //todo throw exception
        }

        template.asyncSend(getDestination(tags), msg, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                //todo log
            }

            @Override
            public void onException(Throwable throwable) {
                //todo throw exception
            }
        });
    }

    private void sendOneWayHandler(String tags, Object msgBody, boolean isNeedMsgKey, String msgKey) {
        try{
            Message msg = buildMsg(msgBody, isNeedMsgKey, msgKey);
            if (null == msg) {
                //todo throw exception
            }
            template.sendOneWay(getDestination(tags), msg);
        }catch(Throwable e){
             //todo throw exception
        }
    }

    private Message buildMsg(Object msgBody, boolean isNeedMsgKey, String msgKey) {
        if (!isNeedMsgKey) {
            return MessageBuilder.withPayload(msgBody).build();
        }

        return MessageBuilder.withPayload(msgBody).setHeader("KEYS", getMsgKey(msgKey)).build();
    }

    private void checkParams(Object msgBody) {
        if (StringUtils.isBlank(topic) || null == msgBody) {
            //todo throw exception
        }
    }

    private String getDestination(String tags) {
        return StringUtils.isBlank(tags) ? topic : topic + ":" + tags;
    }

    public String getMsgKey(String msgKey) {
        return StringUtils.isBlank(msgKey) ? UUID.randomUUID().toString() : msgKey;
    }
}
